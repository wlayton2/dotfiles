#!/bin/bash

# Symlink aliases
ln -sv ~/Development/dotfiles/.bash_aliases ~
ln -sv ~/Development/dotfiles/.tmux.conf ~