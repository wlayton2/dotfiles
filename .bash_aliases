# git
alias g=git
alias gs='git status'
alias gd='git diff'
alias gp='git pull'
alias gc='git commit -m'

# make
alias m=make

# docker
alias d=docker
alias c=docker-compose

# tmux
alias t=tmux

# kubectl
alias k=kubectl

# skaffold
alias s=skaffold

# yarn
alias y=yarn

# Mac OS
alias date=gdate